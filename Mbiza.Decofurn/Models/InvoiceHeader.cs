﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mbiza.Decofurn.Models
{
    public class InvoiceHeader
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int InvoiceId { get; set; }

        public string InvoiceNumber { get; set; }

        public DateTime? InvoiceDate { get; set; }

        public string Address { get; set; }

        public float? InvoiceTotal { get; set; }

        public virtual ICollection<InvoiceLines> InvoiceLines { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mbiza.Decofurn.Models
{
    public class InvoiceLines
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LineId { get; set; }
        
        public string InvoiceNumber { get; set; }

        public string Description { get; set; }

        public float? Quantity { get; set; }

        public float? UnitSellingPriceExVAT { get; set; }
    }
}

﻿using Mbiza.Decofurn.Models;
using Mbiza.Decofurn.Configuration;
using Microsoft.EntityFrameworkCore;

namespace Mbiza.Decofurn.Data
{
    public class DecofurnDbContext : DbContext
    {
        private string _connString = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="DecofurnDbContext" /> class.
        /// </summary>
        public DecofurnDbContext()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DecofurnDbContext"/> class.
        /// </summary>
        /// <param name="connectionStrings">The connection strings.</param>
        public DecofurnDbContext(ConnectionStrings connectionStrings)
        {
            _connString = connectionStrings.DecofurnDBConnection;
        }

        public DbSet<InvoiceHeader> InvoiceHeader { get; set; }

        public DbSet<InvoiceLines> InvoiceLines { get; set; }

        /// <summary>
        /// Called when [configuring].
        /// </summary>
        /// <param name="options">The options.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(_connString);
        }

        /// <summary>
        /// Override this method to further configure the model that was discovered by convention from the entity types
        /// exposed in <see cref="T:Microsoft.EntityFrameworkCore.DbSet`1" /> properties on your derived context. The resulting model may be cached
        /// and re-used for subsequent instances of your derived context.
        /// </summary>
        /// <param name="modelBuilder">The builder being used to construct the model for this context. Databases (and other extensions) typically
        /// define extension methods on this object that allow you to configure aspects of the model that are specific
        /// to a given database.</param>
        /// <remarks>
        /// If a model is explicitly set on the options for this context (via <see cref="M:Microsoft.EntityFrameworkCore.DbContextOptionsBuilder.UseModel(Microsoft.EntityFrameworkCore.Metadata.IModel)" />)
        /// then this method will not be run.
        /// </remarks>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.AddConfiguration(new InvoiceHeaderConfiguration());
            modelBuilder.AddConfiguration(new InvoiceLinesConfiguration());
        }
    }
}

﻿using System;
using System.IO;
using System.Linq;
using Mbiza.Decofurn.Data;
using Mbiza.Decofurn.Common;
using Mbiza.Decofurn.Models;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using Mbiza.Decofurn.Configuration;
using Microsoft.Extensions.Configuration;

namespace Mbiza.Decofurn
{
    class Program
    {
        public static DecoFurnOptions DecoFurnOptions { get; set; }
        public static string FilePath { get; set; }

        public static IConfigurationRoot Configuration { get; set; }

        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        static void Main(string[] args)
        {
            Console.WriteLine("Liso Mbiza started Decofurn Assessment");

            //Read config file
            ReadConfigFile();

            //Initialize Database
            InitializeDatabase();

            //Get invoice data from csv file
            var invoiceList = GetInvoiceData();
            
            //Save invoices
            SaveInvoices(invoiceList);

            Console.WriteLine("Liso Mbiza finished Decofurn Assessment");
            Console.ReadKey();
        }

        /// <summary>
        /// Initializes the database.
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        private static void InitializeDatabase()
        {
            using (var dbContext = new DecofurnDbContext(DecoFurnOptions.ConnectionStrings))
            {
                try
                {
                    Console.WriteLine("Creating database...");
                    dbContext.Database.EnsureCreated();
                }
                catch (SqlException sExc)
                {
                    Console.WriteLine($"A SQL error occured while creating database. See details ({sExc})");
                }
                catch (Exception e)
                {
                    Console.WriteLine($"A SQL error occured while creating database. See details ({e})");
                }                
            }
        }

        /// <summary>
        /// Gets the invoice data.
        /// </summary>
        /// <returns></returns>
        private static List<InvoiceHeader> GetInvoiceData()
        {
            try
            {
                return FileHelper.GetInvoiceData(FilePath);
            }
            catch (Exception e)
            {
                Console.WriteLine($"An error occured while getting invoice data. See details ({e})");
            }
            return new List<InvoiceHeader>();
        }

        /// <summary>
        /// Saves the invoices.
        /// </summary>
        /// <param name="invoices">The invoices.</param>
        /// <exception cref="NotImplementedException"></exception>
        private static void SaveInvoices(List<InvoiceHeader> invoices)
        {
            Console.WriteLine($"{DateTime.Now:yyyy-MM-dd HH:mm:ss} => Started creating invoices...");

            using (var dbContext = new DecofurnDbContext(DecoFurnOptions.ConnectionStrings))
            {
                try
                {
                    Console.WriteLine("###############################################################");
                    Console.WriteLine("Invoice Number \t Total Quantity");
                    foreach (var entity in invoices)
                    {
                        dbContext.InvoiceHeader.Add(entity);
                        foreach (var invoiceLine in entity.InvoiceLines)
                        {
                            dbContext.InvoiceLines.Add(invoiceLine);
                        }
                        Console.WriteLine($"{entity.InvoiceNumber} \t {entity.InvoiceLines.Sum(x => x.Quantity)}");
                    }
                    Console.WriteLine("###############################################################");
                    dbContext.SaveChanges();
                }
                catch (SqlException sExc)
                {
                    Console.WriteLine($"A SQL error occured while saving invoices. See details ({sExc})");
                }
                catch (Exception e)
                {
                    Console.WriteLine($"An error occured while saving invoices. See details ({e})");
                }
            }
            Console.WriteLine("Finished saving invoices");
        }

        /// <summary>
        /// Reads the configuration file.
        /// </summary>
        private static void ReadConfigFile()
        {
            Console.WriteLine("Reading config file");

            try
            {
                var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

                Configuration = builder.Build();

                DecoFurnOptions = Configuration.Get<DecoFurnOptions>();
                FilePath = Configuration.GetValue<string>("FilePath");

                Console.WriteLine($"Current Decofurn Connection String : {DecoFurnOptions.ConnectionStrings.DecofurnDBConnection}");
            }
            catch (Exception e)
            {
                Console.WriteLine($"An error occured while reading config file. See details ({e.ToString()})");
            }
        }
    }
}

﻿using Mbiza.Decofurn.Models;
using System;
using System.Collections.Generic;

namespace Mbiza.Decofurn.Common
{
    public static class Converters
    {
        /// <summary>
        /// Converts to invoiceheader.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <returns></returns>
        public static InvoiceHeader ToInvoiceHeader(this string[] items)
        {
            return new InvoiceHeader
            {
                InvoiceNumber = items[0],
                InvoiceDate = items[1].ToDateTime(),
                Address = items[2].ToString(),
                InvoiceTotal = items[3].ToFloat(),
                InvoiceLines = new List<InvoiceLines> { items.ToInvoiceLine() }
            };
        }

        /// <summary>
        /// Converts to invoiceline.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <returns></returns>
        public static InvoiceLines ToInvoiceLine(this string[] items)
        {
            return new InvoiceLines
            {
                InvoiceNumber = items[0],
                Description = items[4],
                Quantity = items[5].ToFloat(),
                UnitSellingPriceExVAT = items[6].ToFloat(),
            };
        }
    }
}

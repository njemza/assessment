﻿using Mbiza.Decofurn.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Mbiza.Decofurn.Common
{
    public static class FileHelper
    {
        /// <summary>
        /// Gets the invoice data.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns></returns>
        public static List<InvoiceHeader> GetInvoiceData(string filePath)
        {
            int index = 0;
            List<InvoiceHeader> invoices = new List<InvoiceHeader>();
            using (StreamReader reader = new StreamReader(filePath))
            {
                string recordList = reader.ReadToEnd();
                string[] itemArray = recordList.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);

                foreach (string item in itemArray)
                {
                    if (index == 0)
                    {
                        index++;
                        continue;
                    }
                    index++;

                    string[] items = item.ToString().Split(',');
                    if (items.Length <= 6)
                        continue;

                    string invoiceNumber = items[0];
                    var invoice = invoices.FirstOrDefault(x => x.InvoiceNumber == invoiceNumber);
                    if (invoice != default)
                    {
                        invoice.InvoiceLines.Add(items.ToInvoiceLine());
                        continue;
                    }

                    invoices.Add(items.ToInvoiceHeader());
                }
            }
            return invoices;
        }
    }
}

﻿using System;
using System.Globalization;

namespace Mbiza.Decofurn.Common
{
    public static class Utils
    {
        /// <summary>
        /// Converts to float.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static float ToFloat(this string value)
        {
            float.TryParse(value, out float fltValue);
            if (fltValue == 0)
                fltValue = float.Parse(value, CultureInfo.InvariantCulture.NumberFormat);

            return fltValue;
        }

        /// <summary>
        /// Converts to datetime.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static DateTime ToDateTime(this string value)
        {
            DateTime.TryParse(value, out DateTime dateTime);
            if (dateTime == default)
                dateTime = DateTime.ParseExact(value, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);

            return dateTime;
        }
    }
}

﻿using Mbiza.Decofurn.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Mbiza.Decofurn.Configuration
{
    public class InvoiceLinesConfiguration : EntityTypeConfiguration<InvoiceLines>
    {
        /// <summary>
        /// Maps the specified builder.
        /// </summary>
        /// <param name="builder">The builder.</param>
        public override void Map(EntityTypeBuilder<InvoiceLines> builder)
        {
            builder.ToTable("InvoiceLines", schema: "dbo");

            builder.HasKey(p => p.LineId).IsClustered();

            builder.Property(p => p.InvoiceNumber).HasColumnType("varchar(50)").IsRequired();
            builder.Property(p => p.Description).HasColumnType("varchar(50)").IsRequired(false);
            builder.Property(p => p.Quantity).HasColumnType("float").IsRequired(false);
            builder.Property(p => p.UnitSellingPriceExVAT).HasColumnType("float").IsRequired(false);
        }
    }
}

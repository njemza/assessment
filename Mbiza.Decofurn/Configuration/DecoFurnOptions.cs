﻿namespace Mbiza.Decofurn.Configuration
{
    public class DecoFurnOptions
    {
        public ConnectionStrings ConnectionStrings { get; set; }
    }

    public class ConnectionStrings
    {
        public string DecofurnDBConnection { get; set; }
    }
}

﻿using Mbiza.Decofurn.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Mbiza.Decofurn.Configuration
{
    public class InvoiceHeaderConfiguration : EntityTypeConfiguration<InvoiceHeader>
    {
        /// <summary>
        /// Maps the specified builder.
        /// </summary>
        /// <param name="builder">The builder.</param>
        public override void Map(EntityTypeBuilder<InvoiceHeader> builder)
        {
            builder.ToTable("InvoiceHeader", schema: "dbo");

            builder.HasKey(e => e.InvoiceId);
            
            builder.Property(p => p.InvoiceNumber).HasColumnType("varchar(50)").IsRequired();
            builder.Property(p => p.InvoiceDate).HasColumnType("date").IsRequired(false);
            builder.Property(p => p.InvoiceTotal).HasColumnType("float").IsRequired(false);
            builder.Property(p => p.Address).HasColumnType("varchar(50)").IsRequired(false);
        }
    }
}
